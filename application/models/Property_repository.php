<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: dogukan
 * Date: 30.05.2018
 * Time: 23:37
 */

class Property_repository extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function findPropertyByUuid($uuid){
        $query = $this->db->select('*')->from('property')->where('uuid', $uuid);
        $result = $query->get();
        return $result->result_array();
    }

    public function findPropertiesByType($type){
        $query = $this->db->select('*')->from('property')->where('type', $type);
        $result = $query->get();
        return $result->result_array();
    }

    public function findPropertiesByParentUuid($parent_uuid){
        $query = $this->db->select('*')->from('property')->where('parent_uuid', $parent_uuid);
        $result = $query->get();
        return $result->result_array();
    }

    public function findPropertiesByValue($value){
        $query = $this->db->select('*')->from('property')->where('value', $value);
        $result = $query->get();
        return $result->result_array();
    }

    public function save($property){
        $data = array(
                'uuid' => $property->getUuid(),
                'type' => $property->getType(),
                'parent_uuid' => $property->getParentComponentUuid(),
                'value' => $property->getValue()
        );

        $query = $this->db->insert('property', $data);
        return $query;
    }

    public function update($property, $uuid){
        $data = array(
            'type' => $property->getType(),
            'parent_uuid' => $property->getParentComponentUuid(),
            'value' => $property->getValue()
        );

        $query = $this->db->where('uuid', $uuid)->update('property', $data);
        return $query;
    }

    public function delete($uuid){
        $query = $this->db->where('uuid', $uuid)->delete('property');
        return $query;
    }
}