<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: dogukan
 * Date: 30.05.2018
 * Time: 23:37
 */

class Component_repository extends CI_Model
{
    public $uuid;
    public $type;
    public $ownerUuid;
    public $parentUuid;

    public function __construct()
    {
        parent::__construct();
    }

    public function findComponentByUuid($uuid){
        $query = $this->db->select('*')->from('component')->where('uuid', $uuid);
        $result = $query->get();
        return $result->result_array();
    }

    public function findComponentsByType($type){
        $query = $this->db->select('*')->from('component')->where('type', $type);
        $result = $query->get();
        return $result->result_array();
    }

    public function findComponentsByOwnerUuid($owner_uuid){
        $query = $this->db->select('*')->from('component')->where('owner_uuid', $owner_uuid);
        $result = $query->get();
        return $result->result_array();
    }

    public function findComponentByParentUuid($parent_uuid){
        $query = $this->db->select('*')->from('component')->where('parent_uuid', $parent_uuid);
        $result = $query->get();
        return $result->result_array();
    }

    public function save($component){
        $data = array(
                'uuid' => $component->getUuid(),
                'type' => $component->getType(),
                'owner_uuid' => $component->getOwnerUuid(),
                'parent_uuid' => $component->getParentUuid()
        );

        $query = $this->db->insert('component', $data);
        return $query;
    }

    public function update($component, $uuid){
        $data = array(
            'type' => $component->getType(),
            'owner_uuid' => $component->getOwnerUuid(),
            'parent_uuid' => $component->getParentUuid()
        );

        $query = $this->db->where('uuid', $uuid)->update('component', $data);;
        return $query;
    }

    public function delete($uuid){
        $query = $this->db->where('uuid', $uuid)->delete('component');
        return $query;
    }
}