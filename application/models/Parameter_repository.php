<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: dogukan
 * Date: 30.05.2018
 * Time: 23:37
 */

class Parameter_repository extends CI_Model
{
    public function findParameterByUuid($uuid){
        $query = $this->db->select('*')->from('parameter')->where('uuid', $uuid);
        $result = $query->get();
        return $result->result_array();
    }

    public function findParametersByType($type){
        $query = $this->db->select('*')->from('parameter')->where('type', $type);
        $result = $query->get();
        return $result->result_array();
    }

    public function findParametersByParentUuid($parent_uuid){
        $query = $this->db->select('*')->from('parameter')->where('parent_uuid', $parent_uuid);
        $result = $query->get();
        return $result->result_array();
    }

    public function findParameterByValue($value){
        $query = $this->db->select('*')->from('parameter')->where('value', $value);
        $result = $query->get();
        return $result->result_array();
    }

    public function save($parameter){
        $data = array(
            'uuid' => $parameter->getUuid(),
            'type' => $parameter->getType(),
            'parent_uuid' => $parameter->getParentPropertyUuid(),
            'value' => $parameter->getValue()
        );

        $query = $this->db->insert('parameter', $data);
        return $query;
    }

    public function update($parameter, $uuid){
        $data = array(
            'type' => $parameter->getType(),
            'parent_uuid' => $parameter->getParentPropertyUuid(),
            'value' => $parameter->getValue()
        );

        $query = $this->db->where('uuid', $uuid)->update('parameter', $data);
        return $query;
    }

    public function delete($uuid){
        $query = $this->db->where('uuid', $uuid)->delete('parameter');
        return $query;
    }
}