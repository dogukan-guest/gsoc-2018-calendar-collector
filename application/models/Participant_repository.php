<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: dogukan
 * Date: 30.05.2018
 * Time: 23:37
 */

class Participant_repository extends CI_Model
{
    public function findParticipantByUuid($uuid){
        $query = $this->db->select('*')->from('participant')->where('uuid', $uuid);
        $result = $query->get();
        return $result->result_array();
    }

    public function findParticipantByDisplayName($display_name){
        $query = $this->db->select('*')->from('participant')->where('display_name', $display_name);
        $result = $query->get();
        return $result->result_array();
    }

    public function findParticipantByUserName($user_name){
        $query = $this->db->select('*')->from('participant')->where('user_name', $user_name);
        $result = $query->get();
        return $result->result_array();
    }

    public function save($participant){
        $data = array(
            'uuid' => $participant->getUuid(),
            'type' => $participant->getType(),
            'user_name' => $participant->getUserName(),
            'password' => $participant->getPassword(),
            'display_name' => $participant->getDisplayName()
        );

        $query = $this->db->insert('participant', $data);
        return $query;
    }

    public function update($participant, $uuid){
        $data = array(
            'type' => $participant->getType(),
            'user_name' => $participant->getUserName(),
            'password' => $participant->getPassword(),
            'display_name' => $participant->getDisplayName()
        );

        $query = $this->db->where('uuid', $uuid)->update('participant', $data);
        return $query;
    }

    public function delete($uuid){
        $query = $this->db->where('uuid', $uuid)->delete('participant');
        return $query;
    }
}