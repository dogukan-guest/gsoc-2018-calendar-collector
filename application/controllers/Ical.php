<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: dogukan
 * Date: 01.06.2018
 * Time: 06:57
 */

class Ical extends CI_Controller
{
    private $elementService;
    private $participantService;
    private $jsonMapper;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();

        $this->load->model('component_repository');
        $this->load->model('property_repository');
        $this->load->model('parameter_repository');
        $this->load->model('participant_repository');

        $this->load->library('element/componentEntity');
        $this->load->library('element/propertyEntity');
        $this->load->library('element/parameterEntity');
        $this->load->library('element/participantEntity');

        $this->load->library('factory/componentFactory');
        $this->load->library('factory/propertyFactory');
        $this->load->library('factory/parameterFactory');
        $this->load->library('factory/participantFactory');

        $this->load->library('service/elementService');
        $this->load->library('service/participantService');
        $this->load->library('mapper/jsonMapper');
        $this->load->helper('download');

        $this->elementService = new ElementService();
        $this->participantService = new ParticipantService();
        $this->jsonMapper = new JsonMapper();
    }

    public function convertToIcal($componentUuid){
        $component = $this->elementService->getComponentByUuid($componentUuid);
        //var_dump($component);
        $componentProperty = $this->elementService->getPropertiesByParentComponentUuid($component[0]['uuid']);
        //var_dump($componentProperty);
        $output = 'BEGIN:VCALENDAR' . PHP_EOL .  'BEGIN:V' . $component[0]['type'] . PHP_EOL;
        foreach ($componentProperty as $key => $data){
            $output = $output . $data['type'] . ':' . $data['value'] . PHP_EOL;
        }
        $output = $output . 'END:V' . $component[0]['type'] . PHP_EOL . 'BEGIN:VCALENDAR';
        echo $output;
    }

}