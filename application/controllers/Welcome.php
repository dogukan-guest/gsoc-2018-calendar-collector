<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        $this->load->database();
		$this->load->view('welcome_message');

		    $this->load->model('component_repository');
            $this->load->model('property_repository');
            $this->load->model('parameter_repository');
            $this->load->model('participant_repository');

            $this->load->library('element/componentEntity');
            $this->load->library('element/propertyEntity');
            $this->load->library('element/parameterEntity');
            $this->load->library('element/participantEntity');

            $this->load->library('factory/componentFactory');
            $this->load->library('factory/propertyFactory');
            $this->load->library('factory/parameterFactory');
            $this->load->library('factory/participantFactory');

            $this->load->library('service/elementService');
            $this->load->library('service/participantService');
	}
}
