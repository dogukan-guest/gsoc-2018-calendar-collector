<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: dogukan
 * Date: 01.06.2018
 * Time: 02:23
 */

class Component extends CI_Controller
{
    private $elementService;
    private $participantService;
    private $jsonMapper;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();

        $this->load->model('component_repository');
        $this->load->model('property_repository');
        $this->load->model('parameter_repository');
        $this->load->model('participant_repository');

        $this->load->library('element/componentEntity');
        $this->load->library('element/propertyEntity');
        $this->load->library('element/parameterEntity');
        $this->load->library('element/participantEntity');

        $this->load->library('factory/componentFactory');
        $this->load->library('factory/propertyFactory');
        $this->load->library('factory/parameterFactory');
        $this->load->library('factory/participantFactory');

        $this->load->library('service/elementService');
        $this->load->library('service/participantService');
        $this->load->library('mapper/jsonMapper');

        $this->elementService = new ElementService();
        $this->participantService = new ParticipantService();
        $this->jsonMapper = new JsonMapper();
    }

    public function componentPost()
    {
        $this->jsonMapper->setElementService($this->elementService);
        $this->jsonMapper->setParticipantService($this->participantService);
        $component = $this->jsonMapper->fillObjectByJson($this->input->raw_input_stream,'COMPONENT');
        $this->elementService->saveComponent($component);
    }

    public function componentGet($componentUuid){
        $this->elementService->getComponentByUuid($componentUuid);
    }

    public function componentPut($componentUuid){
        $this->jsonMapper->setElementService($this->elementService);
        $this->jsonMapper->setParticipantService($this->participantService);
        $component = $this->jsonMapper->fillObjectByJson($this->input->raw_input_stream,'COMPONENT');
        $this->elementService->updateComponent($component,$componentUuid);
    }

    public function componentDelete($componentUuid){
        $this->elementService->deleteComponentByUuid($componentUuid);
    }
}