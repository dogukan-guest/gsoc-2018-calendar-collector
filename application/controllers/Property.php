<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: dogukan
 * Date: 01.06.2018
 * Time: 02:23
 */

class Property extends CI_Controller
{
    private $elementService;
    private $participantService;
    private $jsonMapper;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();

        $this->load->model('component_repository');
        $this->load->model('property_repository');
        $this->load->model('parameter_repository');
        $this->load->model('participant_repository');

        $this->load->library('element/componentEntity');
        $this->load->library('element/propertyEntity');
        $this->load->library('element/parameterEntity');
        $this->load->library('element/participantEntity');

        $this->load->library('factory/componentFactory');
        $this->load->library('factory/propertyFactory');
        $this->load->library('factory/parameterFactory');
        $this->load->library('factory/participantFactory');

        $this->load->library('service/elementService');
        $this->load->library('service/participantService');
        $this->load->library('mapper/jsonMapper');

        $this->elementService = new ElementService();
        $this->participantService = new ParticipantService();
        $this->jsonMapper = new JsonMapper();
    }

    public function propertyPost(){
        $this->jsonMapper->setElementService($this->elementService);
        $this->jsonMapper->setParticipantService($this->participantService);
        $property = $this->jsonMapper->fillObjectByJson($this->input->raw_input_stream,'PROPERTY');
        $this->elementService->saveProperty($property);
    }

    public function propertyGet($propertyUuid){
        $this->elementService->getPropertyByUuid($propertyUuid);
    }

    public function propertyPut($propertyUuid){
        $this->jsonMapper->setElementService($this->elementService);
        $this->jsonMapper->setParticipantService($this->participantService);
        $property = $this->jsonMapper->fillObjectByJson($this->input->raw_input_stream,'PROPERTY');
        $this->elementService->updateProperty($property, $propertyUuid);
    }

    public function propertyDelete($propertyUuid){
        $this->elementService->deletePropertyByUuid($propertyUuid);
    }
}