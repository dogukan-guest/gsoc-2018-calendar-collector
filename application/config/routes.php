<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';

$route['component']['POST'] = 'component/componentPost';
$route['component/(:any)']['GET'] = 'component/componentGet/$1';
$route['component/(:any)']['PUT'] = 'component/componentPut/$1';
$route['component/(:any)']['DELETE'] = 'component/componentDelete/$1';

$route['property']['POST'] = 'property/propertyPost';
$route['property/(:any)']['GET'] = 'property/propertyGet/$1';
$route['property/(:any)']['PUT'] = 'property/propertyPut/$1';
$route['property/(:any)']['DELETE'] = 'property/propertyDelete/$1';

$route['parameter']['POST'] = 'parameter/parameterPost';
$route['parameter/(:any)']['GET'] = 'parameter/parameterGet/$1';
$route['parameter/(:any)']['PUT'] = 'parameter/parameterPut/$1';
$route['parameter/(:any)']['DELETE'] = 'parameter/parameterDelete/$1';

$route['participant']['POST'] = 'participant/participantPost';
$route['participant/(:any)']['GET'] = 'participant/participantGet/$1';
$route['participant/(:any)']['PUT'] = 'participant/participantPut/$1';
$route['participant/(:any)']['DELETE'] = 'participant/participantDelete/$1';

$route['ical/(:any)'] = 'ical/convertToIcal/$1';

$route['translate_uri_dashes'] = FALSE;
