<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: dogukan
 * Date: 31.05.2018
 * Time: 19:02
 */

class ParticipantService
{
    private $participantFactory;
    private $participantRepository;

    public function __construct()
    {
        $this->participantFactory = new ParticipantFactory();
        $this->participantRepository = new Participant_repository();
    }

    public function createParticipant($participantType){
        return $this->participantFactory->create($participantType);
    }

    public function getParticipantByUuid($participantUuid){
        return $this->participantRepository->findParticipantByUuid($participantUuid);
    }

    public function getParticipantByUserName($participantUserName){
        return $this->participantRepository->findParticipantByUserName($participantUserName);
    }

    public function getParticipantByDisplayName($participantDisplayName){
        return $this->participantRepository->findParticipantByDisplayName($participantDisplayName);
    }

    public function saveParticipant($participant){
        return $this->participantRepository->save($participant);
    }

    public function updateParticipant($participant, $participantUuid){
        return $this->participantRepository->update($participant, $participantUuid);
    }

    public function deleteParticipantByUuid($participantUuid){
        return $this->participantRepository->delete($participantUuid);
    }
}