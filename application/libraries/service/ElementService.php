<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: dogukan
 * Date: 31.05.2018
 * Time: 18:30
 */

class ElementService
{
    private $componentFactory;
    private $propertyFactory;
    private $parameterFactory;
    private $componentRepository;
    private $propertyRepository;
    private $parameterRepository;
    private $participantRepository;

    public function __construct()
    {
        $this->componentFactory = new ComponentFactory();
        $this->propertyFactory = new PropertyFactory();
        $this->parameterFactory = new ParameterFactory();
        $this->componentRepository = new Component_repository();
        $this->propertyRepository = new Property_repository();
        $this->parameterRepository = new Parameter_repository();
        $this->participantRepository = new Participant_repository();
    }


    public function createComponent($componentType){
        return $this->componentFactory->create($componentType);
    }

    public function getComponentByUuid($componentUuid){
        return $this->componentRepository->findComponentByUuid($componentUuid);
    }

    public function getComponentsByType($componentType){
        return $this->componentRepository->findComponentsByType($componentType);
    }

    public function getComponentsByOwnerUserName($componentOwnerUserName){
        $ownerResult = $this->participantRepository->findParticipantByUserName($componentOwnerUserName);
        $ownerUuid = $ownerResult[0]['uuid'];
        return $this->componentRepository->findComponentsByOwnerUuid($ownerUuid);
    }

    public function getComponentsByOwnerUuid($componentOwnerUuid){
        return $this->componentRepository->findComponentsByOwnerUuid($componentOwnerUuid);
    }

    public function getComponentsByParent($componentParentObj){
        return $this->componentRepository->findComponentsByOwnerUuid($componentParentObj->getUuid());
    }

    public function getComponentsByParentUuid($componentParentUuid){
        return $this->componentRepository->findComponentsByOwnerUuid($componentParentUuid);
    }

    public function saveComponent($componentObj){
        return $this->componentRepository->save($componentObj);
    }

    public function updateComponent($componentObj, $uuid){
        return $this->componentRepository->update($componentObj, $uuid);
    }

    public function deleteComponentByUuid($componentUuid){
        return $this->componentRepository->delete($componentUuid);
    }


    public function createProperty($propertyType){
        return $this->propertyFactory->create($propertyType);
    }

    public function getPropertyByUuid($propertyUuid){
        return $this->propertyRepository->findPropertyByUuid($propertyUuid);
    }

    public function getPropertiesByType($propertyType){
        return $this->propertyRepository->findPropertiesByType($propertyType);
    }

    public function getPropertiesByParentComponent($propertyParentObj){
        return $this->propertyRepository->findPropertiesByParentUuid($propertyParentObj->getUuid());
    }

    public function getPropertiesByParentComponentUuid($propertyParentUuid){
        return $this->propertyRepository->findPropertiesByParentUuid($propertyParentUuid);
    }

    public function getPropertiesByValue($propertyValue){
        return $this->propertyRepository->findPropertiesByValue($propertyValue);
    }

    public function saveProperty($property){
        return $this->propertyRepository->save($property);
    }

    public function updateProperty($property, $propertyUuid){
        return $this->propertyRepository->update($property, $propertyUuid);
    }

    public function deletePropertyByUuid($propertyUuid){
        return $this->propertyRepository->delete($propertyUuid);
    }


    public function createParameter($parameterType){
        return $this->parameterFactory->create($parameterType);
    }

    public function getParameterByUuid($parameterUuid){
        return $this->parameterRepository->findParameterByUuid($parameterUuid);
    }

    public function getParametersType($parameterType){
        return $this->parameterRepository->findParametersByType($parameterType);
    }

    public function getParametersByParentProperty($parameterParentObj){
        return $this->parameterRepository->findParameterByUuid($parameterParentObj->getUuid());
    }

    public function getParameterByParentPropertyUuid($parameterParentUuid){
        return $this->parameterRepository->findParameterByUuid($parameterParentUuid);
    }

    public function getParametersByValue($parameterValue){
        return $this->parameterRepository->findParameterByValue($parameterValue);
    }

    public function saveParameter($parameter){
        return $this->parameterRepository->save($parameter);
    }

    public function updateParameter($parameter, $parameterUuid){
        return $this->parameterRepository->update($parameter, $parameterUuid);
    }

    public function deleteParameterByUuid($parameterUuid){
        return $this->parameterRepository->delete($parameterUuid);
    }

}