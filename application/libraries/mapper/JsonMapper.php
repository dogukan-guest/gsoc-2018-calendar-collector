<?php
/**
 * Created by PhpStorm.
 * User: dogukan
 * Date: 01.06.2018
 * Time: 04:24
 */

class JsonMapper
{
    private $elementService;
    private $participantService;

    public function fillObjectByJson($json_input, $object_type){
        switch ($object_type){
            case 'PARTICIPANT' :
                $participant = $this->participantService->createParticipant('');
                $jsonData = json_decode($json_input, true);
                //$participant->setUuid($jsonData['uuid']);
                $participant->setUserName($jsonData['user_name']);
                $participant->setPassword($jsonData['password']);
                $participant->setType($jsonData['type']);
                $participant->setDisplayName($jsonData['display_name']);
                return $participant;
                break;

            case 'COMPONENT' :
                $component = $this->elementService->createComponent('');
                $jsonData = json_decode($json_input, true);
                //$component->setUuid($jsonData['uuid']);
                $component->setType($jsonData['type']);
                $component->setOwnerUuid($jsonData['owner_uuid']);
                $component->setParentUuid($jsonData['parent_uuid']);
                return $component;
                break;

            case 'PROPERTY' :
                $property = $this->elementService->createProperty('');
                $jsonData = json_decode($json_input, true);
                //$property->setUuid($jsonData['uuid']);
                $property->setType($jsonData['type']);
                $property->setParentComponentUuid($jsonData['parent_component_uuid']);
                $property->setValue($jsonData['value']);
                return $property;
                break;

            case 'PARAMETER' :
                $parameter = $this->elementService->createParameter('');
                $jsonData = json_decode($json_input, true);
                //$parameter->setUuid($jsonData['uuid']);
                $parameter->setType($jsonData['type']);
                $parameter->setParentPropertyUuid($jsonData['parent_property_uuid']);
                $parameter->setValue($jsonData['value']);
                return $parameter;
                break;

            default:
                return null;

        }
    }

    /**
     * @return mixed
     */
    public function getParticipantService()
    {
        return $this->participantService;
    }

    /**
     * @param mixed $participantService
     */
    public function setParticipantService($participantService)
    {
        $this->participantService = $participantService;
    }

    /**
     * @return mixed
     */
    public function getElementService()
    {
        return $this->elementService;
    }

    /**
     * @param mixed $elementService
     */
    public function setElementService($elementService)
    {
        $this->elementService = $elementService;
    }


}