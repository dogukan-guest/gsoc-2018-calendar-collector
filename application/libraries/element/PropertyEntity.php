<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: dogukan
 * Date: 30.05.2018
 * Time: 23:30
 */

class PropertyEntity
{
    private $uuid;
    private $type;
    private $parentComponentUuid;
    private $value;
    private $childParameters;

    private $propertyRepository;
    private $parameterRepository;

    public function __construct()
    {
        $this->propertyRepository = new Property_repository();
        $this->parameterRepository = new Parameter_repository();

        $uuid_token = uniqid(mt_rand(), true);
        if(empty($this->propertyRepository->findPropertyByUuid($uuid_token))){
            $this->uuid = $uuid_token;
        }else{
            $this->uuid = $uuid_token = uniqid(mt_rand(), true);
        }
    }

    public function loadChildParameters(){
        $this->childParameters = $this->parameterRepository->findParametersByParentUuid($this->uuid);
    }
    /**
     * @return mixed
     */
    public function getChildParameters()
    {
        return $this->childParameters;
    }

    /**
     * @param mixed $childParameters
     */
    public function setChildParameters($childParameters)
    {
        $this->childParameters = $childParameters;
    }


    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getParentComponentUuid()
    {
        return $this->parentComponentUuid;
    }

    /**
     * @param mixed $parentComponentUuid
     */
    public function setParentComponentUuid($parentComponentUuid)
    {
        $this->parentComponentUuid = $parentComponentUuid;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

}