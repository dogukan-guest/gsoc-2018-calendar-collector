<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: dogukan
 * Date: 30.05.2018
 * Time: 23:30
 */

class ParameterEntity
{
    private $uuid;
    private $type;
    private $parentPropertyUuid;
    private $value;

    private $parameterRepository;

    public function __construct()
    {
        $this->parameterRepository = new Parameter_repository();

        $uuid_token = uniqid(mt_rand(), true);
        if(empty($this->parameterRepository->findParameterByUuid($uuid_token))){
            $this->uuid = $uuid_token;
        }else{
            $this->uuid = $uuid_token = uniqid(mt_rand(), true);
        }
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getParentPropertyUuid()
    {
        return $this->parentPropertyUuid;
    }

    /**
     * @param mixed $parentPropertyUuid
     */
    public function setParentPropertyUuid($parentPropertyUuid)
    {
        $this->parentPropertyUuid = $parentPropertyUuid;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
}