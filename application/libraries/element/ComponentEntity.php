<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: dogukan
 * Date: 30.05.2018
 * Time: 23:30
 */

class ComponentEntity
{
    private $uuid;
    private $type;
    private $ownerUuid;
    private $parentUuid;
    private $childComponents;
    private $childProperties;

    private $componentRepository;
    private $parameterRepository;
    private $propertyRepository;

    public function __construct()
    {
        $this->componentRepository = new Component_repository();
        $this->parameterRepository = new Parameter_repository();
        $this->propertyRepository = new Property_repository();

        $uuid_token = uniqid(mt_rand(), true);
        if(empty($this->componentRepository->findComponentByUuid($uuid_token))){
            $this->uuid = $uuid_token;
        }else{
            $this->uuid = uniqid(mt_rand(), true);
        }
    }


    public function loadChildProperties(){
        $this->childProperties = $this->propertyRepository->findPropertiesByParentUuid($this->uuid);
    }

    /**
     * @return mixed
     */
    public function getChildProperties()
    {
        return $this->childProperties;
    }

    /**
     * @param mixed $childProperties
     */
    public function setChildProperties($childProperties)
    {
        $this->childProperties = $childProperties;
    }


    public function loadChildComponent(){
        $this->childComponents = $this->componentRepository->findComponentByParentUuid($this->uuid);
    }

    /**
     * @return mixed
     */
    public function getChildComponents()
    {
        return $this->childComponents;
    }

    /**
     * @param mixed $childComponents
     */
    public function setChildComponents($childComponents)
    {
        $this->childComponents = $childComponents;
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getOwnerUuid()
    {
        return $this->ownerUuid;
    }

    /**
     * @param mixed $ownerUuid
     */
    public function setOwnerUuid($ownerUuid)
    {
        $this->ownerUuid = $ownerUuid;
    }

    /**
     * @return mixed
     */
    public function getParentUuid()
    {
        return $this->parentUuid;
    }

    /**
     * @param mixed $parentUuid
     */
    public function setParentUuid($parentUuid)
    {
        $this->parentUuid = $parentUuid;
    }

}