<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: dogukan
 * Date: 30.05.2018
 * Time: 23:34
 */

class ParticipantFactory
{
    public static function create($participantType){
        $participant = new ParticipantEntity();
        $participant->setType($participantType);
        return $participant;
    }
}