<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: dogukan
 * Date: 30.05.2018
 * Time: 23:34
 */

class PropertyFactory
{
    public static function create($propertyType){
        $property = new PropertyEntity();
        $property->setType($propertyType);
        return $property;
    }
}