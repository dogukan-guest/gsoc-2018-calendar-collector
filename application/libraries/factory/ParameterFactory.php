<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: dogukan
 * Date: 30.05.2018
 * Time: 23:33
 */

class ParameterFactory
{
    public static function create($parameterType){
        $parameter = new ParameterEntity();
        $parameter->setType($parameterType);
        return $parameter;
    }
}