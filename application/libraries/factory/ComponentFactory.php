<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: dogukan
 * Date: 30.05.2018
 * Time: 23:33
 */

class ComponentFactory
{
    public function create($componentType){
        $component = new ComponentEntity();
        $component->setType($componentType);
        return $component;
    }
}